#include <vector>
#include <limits>       // std::numeric_limits
#include <algorithm>
#include <math.h>

#include "IIndividual.h"
#include "Util.h"

#include "IndividualTrab.h"

#ifndef POPULATION_H
#define POPULATION_H

class Population
{
    public:

        Population();
        virtual ~Population();

        void GenerateRandom(int popLength, int chromoLength);

        void SetLength(int popLength);
        void CalcFitness(int type);

        void SelectParents(float eliteRate, int type);
        void ParentsCrossover(float cRate);
        void ParentsMutation(float pRate, float mRate);

        void SetDistMatrix(float** matrix);

        void GenerateNewPopulation(Population* newPopulation);

        float        GetBestFitness();
        IIndividual* GetBestIndividual();

        void CopyIndividual(int index, IIndividual* individual);
        void PrintIndividuals();

    protected:

    private:

        int         length;
        int         nTeams;
        int         rounds;

        float       bestFitness;
        float       worstFitness;
        float       avgFitness;

        IIndividual*  bestIndividual;
        IIndividual** individuals;

        std::vector<IIndividual*>* bestParents;
        std::vector<IIndividual*>* childrenCrossover;
        std::vector<IIndividual*>* childrenMutation;

        void setBestIndividual(IIndividual* i);

};

#endif // POPULATION_H
