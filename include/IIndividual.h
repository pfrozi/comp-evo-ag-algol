

#ifndef IINDIVIDUAL_H
#define IINDIVIDUAL_H


class IIndividual
{

    public:

        virtual ~IIndividual();

        static IIndividual* Create();

        virtual void  Crossover(IIndividual* individual, IIndividual* newIndividual) = 0;
        virtual void  Mutate(float mRate, IIndividual* newIndividual) = 0;

        virtual void  GenerateRdmAlleles() = 0;

        virtual int   GetAllele(int index)=0;
        virtual void  SetAllele(int index, int value)=0;

        virtual float CheckFitness() = 0;

        virtual int* GetChromo() = 0;

        float fitness;

    protected:

        int        length;
        int*       chromosome;

};

#endif // INDIVIDUAL_H
