
#include "IIndividual.h"
#include <stdlib.h>
#include "Util.h"
#include <math.h>

#define PENALTY_OVERFLOW_POSITIVES 10000
#define PENALTY_OVERFLOW_NEGATIVES 10000

#ifndef INDIVIDUALTRAB_H
#define INDIVIDUALTRAB_H


class IndividualTrab : public IIndividual {

    public:
        IndividualTrab();

        void          Crossover(IIndividual* individual, IIndividual* newIndividual);
        void          Mutate(float mRate, IIndividual* newIndividual);

        void          GenerateRdmAlleles();

        int           GetAllele(int index);
        void          SetAllele(int index, int value);

        float         CheckFitness();

        int*          GetChromo();

    private:

        float baseFunZ(float x, float y);
        float baseFunR(float x, float y);
        float baseFunR1(float x, float y);
        float baseFunRd(float x, float y);
        float funW11(float x, float y);
        float funW3(float x, float y);
        float funW4(float x, float y);


};

#endif // INDIVIDUALTRAB_H
