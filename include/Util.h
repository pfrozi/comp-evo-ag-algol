
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <string.h>
#include <math.h>

#define MY_SEED 314159265

#define CHROMO_LEN 24

#define SOLVE_MIN   0
#define SOLVE_MAX   1

std::vector<std::string> split(std::string str,std::string sep);
float** readMatrix(std::string strMatrix, std::vector<std::string> *header, float** matrix);
bool GetRdmBool(float p);
int  GetRdmInt(int begin, int end);

bool ExistItem(int size, int* v, int value);

void ConvertChromo(int* chromosome, int length, float* x, float* y);
