#include <iostream>
#include <sstream>
#include <list>

#include "Population.h"
#include "Util.h"

#ifndef GATTP_H
#define GATTP_H



class AgAlgol
{
    public:


        AgAlgol();
        virtual ~AgAlgol();


        void Solve(int type);

        double TimeElapsedInMinutes();
        std::string StrTimeElapsed();

        void SetLengthPop(int length);
        void SetPRate(float rate);
        void SetCRate(float rate);
        void SetMRate(float rate);
        void SetEliteRate(float rate);
        void SetStopInGenerations(int n);
        void SetStopInTime(int minutes);


    protected:

    private:

        int solveType;

        int   lengthPop;
        float pRate;
        float cRate;
        float mRate;
        float eliteRate;
        int   stopGen;
        int   stopTime;

        time_t startClock, endClock;

        Population* current;

        long        generation;
        int         genNoImprov;

        void nextGeneration();
        bool verifyStoppage();

        void generateInitial();
        void generateInitial(int length);


};

#endif // GATTP_H
