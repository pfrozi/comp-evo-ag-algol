
#include "../include/Population.h"

Population::Population()
{
    //ctor
    bestParents = new std::vector<IIndividual*>;
    childrenCrossover = new std::vector<IIndividual*>;
    childrenMutation = new std::vector<IIndividual*>;


    individuals     = NULL;
    bestIndividual  = NULL;
}

Population::~Population()
{

    bestParents->clear();

    for(unsigned int i=0;i<childrenCrossover->size();i++) delete childrenCrossover->at(i);
    childrenCrossover->clear();

    for(unsigned int i=0;i<childrenMutation->size();i++) delete childrenMutation->at(i);
    childrenMutation->clear();

    for(int i=0;i<length;i++) delete individuals[i];

    delete individuals;

    delete bestParents;
    delete childrenCrossover;
    delete childrenMutation;


}

void Population::SetLength(int popLength){

    length = popLength;

    if(individuals!=NULL){
        for(int i=0;i<length;i++) delete individuals[i];
        delete individuals;
    }
    if(bestIndividual!=NULL) delete bestIndividual;
    individuals = new IIndividual*[popLength];

    // isso causará leak?
    for(int i=0;i<length;i++){

        individuals[i] = IIndividual::Create();
    }

}

void Population::GenerateRandom(int popLength, int chromoLength){

    //SetLength(popLength);

    for(int l=0;l<length;l++){

        individuals[l]->GenerateRdmAlleles();
    }

}

void Population::CalcFitness(int type){

    float curr = 0, sum = 0;
    bool currBest=0, currWorst=0;

    if(type==SOLVE_MIN){

        bestFitness = std::numeric_limits<float>::max();
        worstFitness = std::numeric_limits<float>::min();
    }else {

        bestFitness = std::numeric_limits<float>::min();
        worstFitness = std::numeric_limits<float>::max();
    }

    for(int i=0;i<length;i++){

        curr = individuals[i]->CheckFitness();


        if(type==SOLVE_MIN){

            currBest  = curr<bestFitness;
            currWorst = curr>worstFitness;
        }else {

            currBest  = curr>bestFitness;
            currWorst = curr<worstFitness;
        }

        if(currBest){

            bestFitness = curr;
            setBestIndividual(individuals[i]);

        }
        else if(currWorst){

            worstFitness = curr;

        }
        sum += curr;

    }
    avgFitness = sum / (float)length;

}

void Population::setBestIndividual(IIndividual* i){

    bestIndividual = i;

}


bool const compareIndividualsMin(const IIndividual* lhs,const IIndividual* rhs)
{

  return lhs->fitness < rhs->fitness;
}
bool const compareIndividualsMax(const IIndividual* lhs,const IIndividual* rhs)
{

  return lhs->fitness > rhs->fitness;
}

void Population::SelectParents(float eliteRate, int type){

    int parentsLen = ceil(length*eliteRate);

    for(int i=0;i<length;i++){

        bestParents->push_back(individuals[i]);

    }

    if(type==SOLVE_MIN){

        std::sort(bestParents->begin(),bestParents->end(),compareIndividualsMin);
    }
    else {

        std::sort(bestParents->begin(),bestParents->end(),compareIndividualsMax);
    }

    bestParents->resize(parentsLen);

}

void Population::ParentsMutation(float pRate, float mRate){

    int mutateLen = ceil(length*pRate);
    int parent1;

    for(int i=0;i<mutateLen;i++){
        parent1 = GetRdmInt(0,bestParents->size()-1);

        IIndividual* newIndividual = IIndividual::Create();

        bestParents->at(parent1)->Mutate(mRate,newIndividual);

        childrenMutation->push_back(newIndividual);
    }

}

void Population::ParentsCrossover(float cRate){

    int crossLen = ceil(length*cRate);
    unsigned int parent1,parent2;

    for(int i=0;i<crossLen;i++){
        parent1 = GetRdmInt(0,bestParents->size()-1);

        if(parent1>(bestParents->size()/2)){
            parent2 = GetRdmInt(0,parent1-1);
        }
        else{
            parent2 = GetRdmInt(parent1+1,bestParents->size()-1);
        }

        IIndividual* newIndividual = IIndividual::Create();

        // crossover entre o individuo de indice parent1 e o individuo de indice parent2
        bestParents->at(parent1)->Crossover(
                                 bestParents->at(parent2)
                                ,newIndividual);

        childrenCrossover->push_back(newIndividual);

    }

}


void Population::GenerateNewPopulation(Population* newPopulation){

    int newLength = bestParents->size()+childrenCrossover->size()+childrenMutation->size();

    newPopulation->SetLength(newLength);

    int k = 0;
    for(unsigned int l=0;l<bestParents->size();l++){
        newPopulation->CopyIndividual(k,bestParents->at(l));
        k++;
    }
    for(unsigned int i=0;i<childrenCrossover->size();i++){
        newPopulation->CopyIndividual(k,childrenCrossover->at(i));
        k++;
    }
    for(unsigned int j=0;j<childrenMutation->size();j++){
        newPopulation->CopyIndividual(k,childrenMutation->at(j));
        k++;
    }

}

float Population::GetBestFitness(){

    return bestFitness;
}

IIndividual* Population::GetBestIndividual(){

    return bestIndividual;
}

void Population::CopyIndividual(int index, IIndividual* individual){

    for(int i=0;i<CHROMO_LEN;i++){
        individuals[index]->SetAllele(i,individual->GetAllele(i));
    }
}
void Population::PrintIndividuals(){

    for(int i=0;i<length;i++){

        printf("\nIndividual %d: ",i);
        for(int j=0;j<CHROMO_LEN;j++){

            printf("%d",individuals[i]->GetAllele(j));
        }
        printf("\nFitness %d: %f",i, individuals[i]->fitness);
    }
}
