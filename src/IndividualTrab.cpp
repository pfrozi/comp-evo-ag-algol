#include "../include/IndividualTrab.h"

IIndividual* IIndividual::Create(){

    return (new IndividualTrab());
}


IndividualTrab::IndividualTrab(){

    length = CHROMO_LEN;

    if(chromosome!=NULL){

        //delete chromosome;
        chromosome = NULL;
    }

    chromosome = new int[length];

    for(int i=0;i<length;i++) chromosome[i]=-1;
}

IIndividual::~IIndividual(){

    delete chromosome;
}

int  IndividualTrab::GetAllele(int index){

    return chromosome[index];
}
void IndividualTrab::SetAllele(int index, int value){

    chromosome[index] = value;
}

void IndividualTrab::Crossover(IIndividual* individual, IIndividual* newIndividual){

    int i=0;
    int cut = (rand()%(length-2))+1;

    for(;i<cut;i++){

        newIndividual->SetAllele(i, chromosome[i]);
    }
    for(int k=i;k<length;k++){

        newIndividual->SetAllele(k, individual->GetAllele(k));
    }

}
void IndividualTrab::Mutate(float mRate, IIndividual* newIndividual){

    for(int i=0;i<length;i++){

        if(GetRdmBool(mRate)){

            newIndividual->SetAllele(i, (chromosome[i]+1)%2);
        }else{

            newIndividual->SetAllele(i, chromosome[i]);
        }
    }
}


void IndividualTrab::GenerateRdmAlleles(){

    for(int i=0;i<length;i++){

        chromosome[i] = rand()%2;
    }
}

int* IndividualTrab::GetChromo(){

    return chromosome;
}


float IndividualTrab::CheckFitness(){

    float x=0, y=0;
    int sign_x=0, sign_y=length/2;

    for(int i=1;i<length/2;i++){

        x += pow(2,(length/2-1)-i) * chromosome[i];
    }

    if(chromosome[sign_x]==1){
        x = x*-1.0f;
    }

    for(int j=length/2+1;j<length;j++){

        y += pow(2,(length-1)-j) * chromosome[j];
    }

    if(chromosome[sign_y]==1){
        y = y*-1.0f;
    }

    x = x/1000.0f;
    y = y/1000.0f;

    fitness = funW11(x,y);

    if(x>2.0f){

        fitness=fitness+PENALTY_OVERFLOW_POSITIVES;

    }else if(x<-2.0f){

        fitness=fitness+PENALTY_OVERFLOW_NEGATIVES;
    }

    if(y>2.0f){

        fitness=fitness+PENALTY_OVERFLOW_POSITIVES;

    }else if(y<-2.0f){

        fitness=fitness+PENALTY_OVERFLOW_NEGATIVES;
    }

    return fitness;
}

float IndividualTrab::baseFunZ(float x, float y){

    return -1.0f*x*sin(sqrt(fabs(x)))-y*sin(sqrt(fabs(y)));
}
float IndividualTrab::baseFunR(float x, float y){

    return 100.0f*pow((y-pow(x,2)),2)+pow(1-x,2);
}
float IndividualTrab::baseFunR1(float x, float y){

    return pow((y-x*x),2)+pow(1-x,2);
}
float IndividualTrab::baseFunRd(float x, float y){

    return 1.0f+baseFunR1(x,y);
}
float IndividualTrab::funW3(float x, float y){

    return baseFunR(x,y)-baseFunZ(x,y);
}
float IndividualTrab::funW4(float x, float y){

    return sqrt(pow(baseFunR(x,y),2)+pow(baseFunZ(x,y),2));
}
float IndividualTrab::funW11(float x, float y){

    return pow(funW3(x,y),2)+funW4(x,y);
}
