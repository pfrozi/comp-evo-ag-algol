#include "../include/AgAlgol.h"
#include "../include/Util.h"

#define RETURN_SUCESS      0
#define RETURN_NOTHING     0
#define RETURN_ERROR       1
#define ERROR_INVALID_ARGS 2

#define ARGS_MAX        7

#define ARGS_HELP       "-h"  // Option para imprimir a lista de opcoes
#define ARGS_RATE_P     "-P"
#define ARGS_RATE_C     "-C"
#define ARGS_RATE_M     "-M"
#define ARGS_RATE_ELITE "-E"

#define ARGS_POP_LEN    "-L"

#define ARGS_STOP_GEN   "-g"
#define ARGS_STOP_TIME  "-t"

float rate_p, rate_c, rate_m, rate_elite;
int stop_gen, stop_time;
int length_pop;

AgAlgol* ga;

void help(){

    printf("\n\n ***** TODO *****\n\n");

}

void setGa(){

    ga->SetPRate(rate_p);
    ga->SetCRate(rate_c);
    ga->SetMRate(rate_m);
    ga->SetEliteRate(rate_elite);
    ga->SetLengthPop(length_pop);
    ga->SetStopInGenerations(stop_gen);
    ga->SetStopInTime(stop_time);

    srand(MY_SEED);
}

int main(int argc, char* argv[]){

    ga = new AgAlgol;

    if (argc < ARGS_MAX || (argc==1 && !(strcmp(argv[1],ARGS_HELP)==0) )) {

        help();
        return (ERROR_INVALID_ARGS);

    } else {

        for (int i = 1; i < argc; i+=2) {

            if (i + 1 != argc)
            {
                std::string arg = argv[i+1];

                if (strcmp(argv[i],ARGS_HELP) == 0) {

                    help();
                    return (RETURN_NOTHING);


                } else if (strcmp(argv[i], ARGS_HELP)==0) {

                    help();

                } else if (strcmp(argv[i], ARGS_RATE_P)==0) {

                    rate_p = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_C)==0) {

                    rate_c = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_M)==0) {

                    rate_m = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_RATE_ELITE)==0) {

                    rate_elite = atof(arg.c_str());

                } else if (strcmp(argv[i], ARGS_STOP_GEN)==0) {

                    stop_gen = atoi(arg.c_str());

                } else if (strcmp(argv[i], ARGS_STOP_TIME)==0) {

                    stop_time = atoi(arg.c_str());

                } else if (strcmp(argv[i], ARGS_POP_LEN)==0) {

                    length_pop = atoi(arg.c_str());

                } else {

                    return (ERROR_INVALID_ARGS);

                }
            }
        }

    }

    setGa();

    ga->Solve(SOLVE_MIN);

    return RETURN_SUCESS;
}

