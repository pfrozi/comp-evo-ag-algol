#include "../include/AgAlgol.h"

AgAlgol::AgAlgol(){

    cRate = 0;
    pRate = 0;

    generation = 1;
    genNoImprov = 0;

    current = new Population;
    startClock = clock();
}


AgAlgol::~AgAlgol(){

}

void AgAlgol::SetLengthPop(int length){

    lengthPop = length;
}

void AgAlgol::generateInitial(int length){

    SetLengthPop(length);
    generateInitial();
}

void AgAlgol::generateInitial(){

    current->SetLength(lengthPop);
    current->GenerateRandom(lengthPop, CHROMO_LEN);
}

void AgAlgol::Solve(int type){

    solveType = type;

    generateInitial();
    current->CalcFitness(solveType);

    do{

        //current->PrintIndividuals();

        // Realiza a selecao dos pais
        current->SelectParents(eliteRate, solveType);

        // Realiza a reproducao por mutacao E crossover
        current->ParentsMutation(pRate, mRate);
        current->ParentsCrossover(cRate);

        nextGeneration();

        endClock = clock();

    }while(!verifyStoppage()); // verifica condicao de parada

    /*
    std::cout << "Tempo Total: ";
    std::cout << StrTimeElapsed() << std::endl;
    std::cout << "Total de geracoes: ";
    std::cout << generation << std::endl;
    std::cout << "**** Solucao **** ";
    std::cout << "Fitness: ";
    std::cout << current->GetBestIndividual()->fitness << std::endl;
    */

    float x;
    float y;

    ConvertChromo(current->GetBestIndividual()->GetChromo(),CHROMO_LEN,&x,&y);

    //printf("Pop_Inicial CrossOver Mutacao Elitismo Taxa_Mutacao Tempo Geracoes Fitness ValorX ValorY");
    printf("%d\t%2.2f\t%2.2f\t%2.2f\t%2.2f\t%.5f\t%d\t%.3f\t%.3f\t%.3f"
            , lengthPop
            , cRate
            , pRate
            , eliteRate
            , mRate
            , ((float)endClock-startClock)/CLOCKS_PER_SEC
            , (int)generation
            , current->GetBestIndividual()->fitness
            , x
            , y);

    delete current;
}

void AgAlgol::nextGeneration(){

    Population* nextPop = new Population;

    current->GenerateNewPopulation(nextPop);

    generation++;

    nextPop->CalcFitness(solveType);

    bool noImprove = false;

    if(solveType==SOLVE_MIN){

        noImprove = nextPop->GetBestFitness()>=current->GetBestFitness();
    }
    else{

        noImprove = nextPop->GetBestFitness()<=current->GetBestFitness();
    }

    if(noImprove) {
        genNoImprov++;
    }
    else{
        genNoImprov = 0;
    }

    delete current;
    current = nextPop;

}

double AgAlgol::TimeElapsedInMinutes(){

    return (((float)endClock-startClock)/CLOCKS_PER_SEC)/60.0;
}

std::string AgAlgol::StrTimeElapsed(){

    std::stringstream out;

    out << ((float)endClock-startClock)/CLOCKS_PER_SEC;
    out << " segundos";

    return out.str();
}


void AgAlgol::SetPRate(float rate){

    pRate = rate;
    // eliteRate  = 1.0f - (pRate+cRate);
}

void AgAlgol::SetCRate(float rate){

    cRate = rate;
    // eliteRate  = 1.0f - (pRate+cRate);
}

void AgAlgol::SetMRate(float rate){

    mRate = rate;
}

void AgAlgol::SetEliteRate(float rate){

    eliteRate = rate;
}

void AgAlgol::SetStopInGenerations(int n){

    stopGen = n;
}

void AgAlgol::SetStopInTime(int minutes){

    stopTime = minutes;
}

bool AgAlgol::verifyStoppage(){

    // Parada por tempo OU parada por quantidade de geracoes sem melhora
    bool time = (TimeElapsedInMinutes()>stopTime);
    bool gen = (genNoImprov>stopGen);

    return(time || gen);
}
